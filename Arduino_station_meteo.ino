#include <Arduino_LPS22HB.h>
#include <Arduino_HTS221.h>

void setup() {
  
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Started");

  if (!BARO.begin()) {
    Serial.println("Failed to initialize pressure sensor!");
    while (1);
  }

  if (!HTS.begin()) {
    Serial.println("Failed to initialize humidity temperature sensor!");
    while (1);
  }

  float pressure = BARO.readPressure();
  float temperature = HTS.readTemperature();
  float humidity = HTS.readHumidity();


  Serial.print("Température = ");
  Serial.print(temperature);
  Serial.println(" °C");

  Serial.print("Pression = ");
  Serial.print(pressure);
  Serial.println(" kPa");

  Serial.print("Humidité = ");
  Serial.print(humidity);
  Serial.println(" %");

  Serial.println();
}

void loop() {
  
//  float pressure = BARO.readPressure();
//  float temperature = HTS.readTemperature();
//  float humidity = HTS.readHumidity();
//
//  Serial.print("Pressure = ");
//  Serial.print(pressure);
//  Serial.println(" kPa");
//
//  Serial.print("Temperature = ");
//  Serial.print(temperature);
//  Serial.println(" °C");
//
//  Serial.print("Humidity = ");
//  Serial.print(humidity);
//  Serial.println(" %");
//
//  Serial.println();
//
//  delay(1000);
}
